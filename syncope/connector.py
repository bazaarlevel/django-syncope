import syncope.apiconnect as syncope
import os

def get_admin_users():
    
    users = []
    SYNCOPE_URL=os.getenv('SYNCOPE_URL')
    syn = syncope.Syncope(syncope_url=SYNCOPE_URL, username=os.getenv('SYNCOPE_USERNAME'), password=os.getenv('SYNCOPE_PASSWORD'))
    for user in  syn.get_users()['result']:
        print (user)
        username = user['username']
        password = user['password']

        
        if user['dynRealms']:
            for group in user['dynRealms']:
                print(group)
                if group ==  "superuser":
                    user = {'username': username, 'password': password}
                    users.append(user)
                    print(user)

    return users
                

