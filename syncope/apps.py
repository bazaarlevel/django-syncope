from django.apps import AppConfig


class SynscopeConfig(AppConfig):
    name = 'synscope'
